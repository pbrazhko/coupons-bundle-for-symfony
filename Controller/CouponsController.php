<?php

namespace CMS\CouponsBundle\Controller;

use CMS\CouponsBundle\Entity\Coupons;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CouponsController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        return $this->render('CouponsBundle:Coupons:list.html.twig', array(
            'coupons' => $this->get('cms.coupons.service')->findAll()
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, $id)
    {
        $service = $this->get('cms.coupons.service');

        $form = $service->generateForm($service->findOneById($id));

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->create($form->getData());

                return $this->redirect($this->generateUrl('cms_coupons_list'));
            }
        }

        return $this->render('CouponsBundle:Coupons:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createAction(Request $request)
    {
        $service = $this->get('cms.coupons.service');

        $form = $service->generateForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $service->create($form->getData());

                return $this->redirect($this->generateUrl('cms_coupons_list'));
            }
        }

        return $this->render('CouponsBundle:Coupons:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $service = $this->get('cms.coupons.service');

        $service->delete($id);

        return $this->redirect($this->generateUrl('cms_coupons_list'));
    }
}
