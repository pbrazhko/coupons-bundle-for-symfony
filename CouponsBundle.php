<?php

namespace CMS\CouponsBundle;

use CMS\CoreBundle\Interfaces\CoreBundleInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CouponsBundle extends Bundle implements CoreBundleInterface
{
    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return true;
    }

    /**
     * @return array
     */
    public function getDescription()
    {
        return array(
            array(
                'title' => 'Coupons',
                'defaultRoute' => 'cms_coupons_list'
            )
        );
    }

}
