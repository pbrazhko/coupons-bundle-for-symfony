<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 03.06.15
 * Time: 17:35
 */

namespace CMS\CouponsBundle\Services;


use CMS\CoreBundle\AbstractCoreService;
use CMS\CouponsBundle\Form\CouponsType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;

class CouponsService extends AbstractCoreService
{
    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            new CouponsType(),
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'CouponsBundle:Coupons';
    }

}