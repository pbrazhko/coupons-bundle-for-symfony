<?php

namespace CMS\CouponsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CouponsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'cms_localization_text_type')
            ->add('price')
            ->add('is_published')
            ->add('is_deleted')
            ->add('photos', 'cms_gallery_images_type');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CMS\CouponsBundle\Entity\Coupons'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cms_couponsbundle_coupons';
    }
}
